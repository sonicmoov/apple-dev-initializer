# apple dev initializer

AppleDeveloperProgramにて、ApplicationIDなどの登録を一括で行なうツールです。

## Installation

```sh
brew tap sonicmoov/apple-dev-initializer git@bitbucket.org:sonicmoov/apple-dev-initializer.git
brew install sonicmoov/apple-dev-initializer/apple-app-init
```

## Usage

```sh
apple-app-init \
  -u [apple-developer-account@examle.com] \
  -a [application id] \
  -n [application name] \
  -s [application id suffixes, for exampel "stg,dev"] \             # (optional)
  -g [application group] \                                          # (optional)
  -c [application child id suffixes, for exampel "child1,child2"] \ # (optional)
  -p [enable push notification service] \                           # (optional)
  -d [do dry run]                                                   # (optional)
```

## Coution

**！！！これはPrivateリポジトリではないので、機密情報などの重要な情報が実装に組み込まれないよう十分に注意して実装してください！！！**
