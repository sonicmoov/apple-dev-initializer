# Documentation: https://docs.brew.sh/Formula-Cookbook
#                https://rubydoc.brew.sh/Formula
# PLEASE REMOVE ALL GENERATED COMMENTS BEFORE SUBMITTING YOUR PULL REQUEST!
class AppleAppInit < Formula
  desc "Appleアプリケーションの初期設定を行なう"
  homepage "https://bitbucket.org/sonicmoov/apple-dev-initializer/src/master/"
  version "0.2.2"
  sha256 "9d954b32003d7ed85cdfafc30fa7e00c58b28acdda1664a3a5f870785f6ecccf"
  url "https://bitbucket.org/sonicmoov/apple-dev-initializer/get/0.2.2.tar.gz"

  def install
    bin.install "apple-app-init"
  end
end

